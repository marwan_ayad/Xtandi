﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class TrackingManager : MonoBehaviour, ITrackableEventHandler {
	private TrackableBehaviour mTrackableBehaviour;
	public bool hideOnLost = false;
	[SerializeField]
	GameObject floatingAnimation;

	// Use this for initialization
	void Start () {
		Vuforia.CameraDevice.Instance.SetFocusMode(Vuforia.CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	#region ITrackableEventHandler implementation

	public void OnTrackableStateChanged (TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			//found
			if(floatingAnimation != null){
				Debug.Log("hello i have found the marker!!!!!!!!!!!!!!");
				floatingAnimation.SetActive(true);
			}
		}
		else
		{
			//lost
			if(floatingAnimation != null && hideOnLost){
				Debug.Log("hello i have lost the marker!!!!!!!!!!!!!!");
				floatingAnimation.SetActive(false);
			}
		}
	}

	#endregion
}
