﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
public class Anim_Trigger : MonoBehaviour {

	public GameObject MainMenu;
	public GameObject SlideNumber;
	public GameObject VirusOut;
	public GameObject[] Slides;
	public void Disable_Virus_sprite()
	{
		MainMenu.SetActive (true);
		Destroy (VirusOut);
	}
	public void StopAnimation()
	{
		gameObject.GetComponent<Animator> ().speed=0;
	}

	public void StartAnimation()
	{

//		for (int i = 0; i < Slides.Length; i++) {
//			Slides [i].GetComponent<Image> ().sprite = null;
//		}
		gameObject.GetComponent<Animator> ().speed=1;
	}
	public void PlayingNextAnimation()
	{
		SlideNumber.SetActive (true);
		MainMenu.SetActive (false);
	}
	public void SelectAnimation(GameObject Slide)
	{
		SlideNumber = Slide;

	}

}
