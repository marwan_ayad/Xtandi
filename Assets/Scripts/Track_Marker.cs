﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class Track_Marker : MonoBehaviour,ITrackableEventHandler {

	private TrackableBehaviour mTrackableBehaviour;
	public GameObject canvas;
	public GameObject PlayMovie;
	 MediaPlayerCtrl scrMedia;
	bool m_bFinish;
	void Start () {
		scrMedia = PlayMovie.GetComponent<MediaPlayerCtrl> ();
		scrMedia.OnEnd += OnEnd;
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
	}
	

	void OnEnd () {
		
		m_bFinish = true;
	}

	#region ITrackableEventHandler implementation

	public void OnTrackableStateChanged (TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			canvas.SetActive (true);
			PlayMovie.SetActive (true);
			scrMedia.m_bLoop = true;
			scrMedia.Load("VirusOut.mp4");
			m_bFinish = false;
//			MoviePlayer.Play ();
			print (1);
			OnTrackingFound ();
		} 
		else 
		{
			PlayMovie.SetActive (false);
			canvas.SetActive (false);
			scrMedia.UnLoad ();
			scrMedia.m_bLoop = false;
			print (0);
			OnTrackingLost ();
		}

	}

#endregion

	private void OnTrackingFound()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = true;
		}

		// Enable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = true;
		}

		Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
	}


	private void OnTrackingLost()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

		// Disable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = false;
		}

		// Disable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = false;
		}

		Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
	}


}
